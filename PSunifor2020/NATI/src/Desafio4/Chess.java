package Desafio4;

public class Chess {
    public static void main(String[] args) {
        //Matriz identificadora
        String[][] pecasNomes = {
                {"Vazio", "0"},
                {"Peão", "1"},
                {"Bispo", "2"},
                {"Cavalo", "3"},
                {"Torre", "4"},
                {"Rainha", "5"},
                {"Rei", "6"}
        };
        //Array que armazena s quantidades
        int[] pecas = {0,0,0,0,0,0,0};

        //Array  do tabuleiro (8 por 8)
       int[][] tabuleiro;
       tabuleiro = new int[][]{{4, 3, 2, 5, 6, 2, 3, 4},
        {1, 1, 1, 1, 1, 1, 1, 1},
        {0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0},
        {1, 1, 1, 1, 1, 1, 1, 1},
        {4, 3, 2, 5, 6, 2, 3, 4}};

      //Contador das peças
       for(int i = 0; i < 8;i++){
           for(int j = 0; j < 8;j++){
               pecas[tabuleiro[i][j]]++;
           }
       }
       //Saída mostrando a quantidade de cada peça
       for (int w = 1; w < 7; w++) {
           System.out.println(pecasNomes[w][0]+": " + pecas[Integer.parseInt(pecasNomes[w][1])] + " peça(s)");
       }
    }
}
