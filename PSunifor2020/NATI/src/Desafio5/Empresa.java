package Desafio5;
import java.util.ArrayList;
public class Empresa {
    //ArrayList dos cargos
    private final ArrayList<Integer> cargos = new ArrayList<Integer>();

    //array auxiliar
    private String[] cargosSTR;

    //ArrayList dos funcionários
    private final ArrayList<Funcionario> employees = new ArrayList<Funcionario>();

    //cadstro de um único cargo
    public void CadastrarCargo(int value) {
        cargos.add(value);
    }

    //cadastro de múltiplos cargos
    public void CadastrarCargos(String valores) {
        cargosSTR = valores.split(" ");
        for (int i = 0; i < cargosSTR.length; i++) {
            cargos.add(Integer.parseInt(cargosSTR[i]));
        }
    }

    //cadastro do funcionário
    public void CadastrarFuncionário(String Nome, int Codigo, int CodigoCargo) throws Exception {

        //checagem do código do funcionário
        boolean repetido = false;
        for(int j = 0; j < employees.size(); j++) {
            if(employees.get(j).getCodigo() == Codigo) {
                repetido = true;
                break;
            }
        }
        if(repetido == true) {
            throw new Exception("Funcionário já cadastrado");
        }

        //checagem da existência do Cargo
        if(CodigoCargo >= cargos.size()) throw new Exception("Cargo inexistente");

        //Adição do funcionário ao vetor de funcionários
        Funcionario f = new Funcionario(Nome, Codigo, CodigoCargo);
        employees.add(f);

    }

    //Checagem de salario por cargo ( quantidade de cargos * salario do cargo)
    public int SalárioPerCargo(int Codigo) throws Exception {
        if(Codigo >= cargos.size()) {
            throw new Exception("Cargo inexistente");
        }
        int soma = 0;
        for(int m = 0; m < employees.size(); m++) {
            if(Codigo == employees.get(m).getCodigo_cargo()) {
                soma += cargos.get(Codigo);
            }
        }
        return soma;
    }
    //relatório final
    public void Relatorio() {
        if(employees.size() != 0)
        for(int num = 0; num < employees.size(); num++) {
            System.out.println("Codigo: " + employees.get(num).getCodigo());
            System.out.println("Nome: " + employees.get(num).getNome());
            System.out.println("Salario: " + cargos.get(employees.get(num).getCodigo_cargo()));
            System.out.println(" ");
        }
        else{
            System.out.println("Não há funcionários");
        }
    }
}
