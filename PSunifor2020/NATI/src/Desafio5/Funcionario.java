package Desafio5;

public class Funcionario {
    private String Nome;
    private int Codigo;
    private int Codigo_cargo;

    //constructor do funcionário
    public Funcionario(String Nome, int Codigo, int Codigo_cargo) {
        this.Nome = Nome;
        this.Codigo = Codigo;
        this.Codigo_cargo = Codigo_cargo;

    }
    //getters e setters
    public String getNome() {
        return this.Nome;
    }
    public int getCodigo() {
        return this.Codigo;
    }
    public int getCodigo_cargo() {
        return this.Codigo_cargo;
    }

    public void setCodigo_cargo(int Codigo_cargo) {
        this.Codigo_cargo = Codigo_cargo;

    }
    public void setCodigo(int Codigo) {
        this.Codigo = Codigo;
    }

    public void setNome(String Nome) {
        this.Nome = Nome;

    }
}

