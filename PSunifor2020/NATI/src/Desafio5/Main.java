package Desafio5;
import java.util.Scanner;
public class Main {

	public static void main(String[] args) throws Exception {
		Scanner a = new Scanner(System.in);
		Empresa e = new Empresa();
		String flag = "s";
		String flag2 = "s";
		String flag3 = "s";
		int op = 0;
		while (op != 5) {
			if (op == 0) {
				System.out.println("Digite 1 para cadastrar o cargo:");
				System.out.println("Digite 2 para cadastrar o funcionãrio:");
				System.out.println("Digite 3 para ver o relatório");
				System.out.println("Digite 4 para cadastrar o cargo:");
				System.out.println("Digite 5 para fechar");

				op = a.nextInt();

			}
			if (op == 1) {
				while (flag.equals("s")) {
					System.out.println("Cadastre o valor do cargo:");
					e.CadastrarCargo(a.nextInt());
					System.out.println("Deseja adicionar outro cargo? (s/n)");
					flag = a.next();
				}
				op = 0;
			}
			if (op == 2) {
				while (flag2.equals("s")) {
					System.out.println("Escreva o nome, o código, e o código do cargo existente do funcionário respectivamente:");
					e.CadastrarFuncionário(a.next(), a.nextInt(), a.nextInt());
					System.out.println("Deseja adicionar outro funcionário? (s/n)");
					flag2 = a.next();
				}
				op = 0;
			}
			if (op == 3) {
				System.out.println("Relatório de Funcionários: ");
				e.Relatorio();
				op = 0;
			}
			//coloque o código do cargo desejado para ver o salário total:
			if (op == 4) {
				while (flag3.equals("s")) {
					System.out.println("Digite o cargo o qual deseja examinar o salário total: ");
					System.out.println(e.SalárioPerCargo(a.nextInt()));
					System.out.println("Deseja ver o salário total de outro cargo? (s/n)");
					flag3 = a.next();
				}
				op = 0;
			}
		}
	}
}