package Desafio6;
import java.util.Scanner;
public class maiorSequencia {
    public static void main(String[] args) throws Exception {
        
        Scanner dado = new Scanner(System.in);
       //adicionando os dados de entrando usando um vetor principal e um auxiliar
        String[] aux = dado.nextLine().split(", ");
        int[] Bin = new int[aux.length];
        for(int i = 0; i < aux.length; i++) {
            Bin[i] = Integer.parseInt(aux[i]);
        }
        //variaveis auxiliares
        int sequencia;
        int maiorSequencia = 0;
        int index = 0;


        for(int j = 0; j < Bin.length; j++){
            sequencia = 0;

            //checagem das posições dos números 0 dentro do vetor principal
            
            //condicional para se o j for o primeiro index do vetor
            if(j ==0) {
                if (Bin[0] == 0) {
                    for (int w = 1; w < Bin.length; w++) {
                        if (Bin[w] == 1) {
                            sequencia++;
                        }
                        else {
                            break;
                        }

                    }
                    maiorSequencia = sequencia;
                }
            }
            //condicional para se o j for o ultimo index do vetor
            if(j == Bin.length - 1){
                    if (Bin[Bin.length - 1] == 0){
                        //loop para checar qual a maior sequencia possível caso o 0 da posição j vire 1
                        for(int w = Bin.length - 2; w > 0; w--){
                            if(Bin[w] == 1){
                                sequencia++;
                            }
                            else{
                                break;
                            }

                        }
                        //condicional para quando a sequencia recém-tirada for a maior apresentada, a posição é armazenada
                        if(maiorSequencia < sequencia){
                            maiorSequencia = sequencia;
                            index = j;
                        }
                    }
            }
            else{
                //loop dos numeros a direita
                for (int w = j + 1; w < Bin.length; w++) {
                    if (Bin[w] == 1) {
                        sequencia++;
                    } else {
                        break;
                    }
                }
                //loop dos numeros a esquerda
                for (int w = j - 1; w > 0; w--) {
                    if (Bin[w] == 1) {
                        sequencia++;
                    } else {
                        break;
                    }

                }
                //condicional para a sequencia recem tirada e armazena-la em outra variavel caso ela seja a maior de todas encontradas até o momento
                if(maiorSequencia < sequencia){
                    maiorSequencia = sequencia;
                    index = j;
                }
            }
            
        }
        //saída do valor da posição do zero que gera a maior sequência
        System.out.println(index + 1);


    }
}
