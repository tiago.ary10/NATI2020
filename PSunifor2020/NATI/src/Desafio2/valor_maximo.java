package Desafio2;
import java.util.Scanner;

public class valor_maximo {
    public static void main(String[] args) {
        Scanner dado = new Scanner(System.in);

        //coloque os dois números
        int a = dado.nextInt();
        int b = dado.nextInt();

        //convertendo o tipo das variáveis de int para double
        double num1 = Double.parseDouble(String.valueOf(a));
        double num2 = Double.parseDouble(String.valueOf(b));

        //soma da média aritmética dos dois números com o valor absoluto da média aritmética dos dois números estando um deles com o sinal trocado
        double formula =(num1+num2)/2 +  Math.abs((num1-num2)/2);
        //convertendo a fórmula para tipo inteiro
        System.out.println((int) formula);
    }
}
