package Desafio3;
import java.util.ArrayList;
import java.util.Scanner;
public class maior_produto {
    public static void main(String[] args) {

        //array principal
        ArrayList<Integer> numbers = new ArrayList<Integer>();

        //array auxiliar
        String[] STRnum;

        //contador de números negativos
        int quantNeg = 0;

        //variável auxiiar pra armazenar o maior numero negativo do array
        int maiorNeg = -100000;
        Scanner dado = new Scanner(System.in);
        STRnum = dado.nextLine().split(" ");

        //adicionando valores da var auxiliar a var principal (valores diferentes de zero)
        for (int i = 0; i < STRnum.length; i++) {
            int numero = Integer.parseInt(STRnum[i]);
            if (numero != 0) {
                numbers.add(numero);
            }
            //condicional para se o número for negativo e for maior que o presente na variavel maiorNeg
            if(numero < 0){
                quantNeg++;
               if(numero > maiorNeg) {
                   maiorNeg = numero;
               }
            }


        }
       //condicional para se a quantidade de números negativos for impar o maior número negativo seja removido
        if(quantNeg % 2 != 0){
            numbers.remove(numbers.indexOf(maiorNeg));
        }

        //saída de dados
        for(int j = 0; j < numbers.size(); j++){
            System.out.print(numbers.get(j) + " ");
        }
    }
}